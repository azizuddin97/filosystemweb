<!DOCTYPE html>
<!-- registered_user_view.php -> user view of Filo System -->
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="author" content="Aziz Uddin">
  <title>Registered User</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <style>
  #wrapper {
    padding-left: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  }

  #wrapper.toggled {
    padding-left: 250px;
  }

  #sidebar-wrapper {
    z-index: 1000;
    position: fixed;
    left: 250px;
    width: 0;
    height: 100%;
    margin-left: -250px;
    overflow-y: auto;
    background: #000;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  }

  #wrapper.toggled #sidebar-wrapper {
    width: 250px;
  }

  #page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 15px;
  }

  #wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -250px;
  }

  /* Sidebar Styles */

  .sidebar-nav {
    position: absolute;
    top: 0;
    width: 250px;
    margin: 0;
    padding: 0;
    list-style: none;
  }

  .sidebar-nav li {
    text-indent: 20px;
    line-height: 40px;
  }

  .sidebar-nav li a {
    display: block;
    text-decoration: none;
    color: #999999;
  }

  .sidebar-nav li a:hover {
    text-decoration: none;
    color: #fff;
    background: rgba(255,255,255,0.2);
  }

  .sidebar-nav li a:active,
  .sidebar-nav li a:focus {
    text-decoration: none;
  }

  .sidebar-nav > .sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
  }

  .sidebar-nav > .sidebar-brand a {
    color: #999999;
  }

  .sidebar-nav > .sidebar-brand a:hover {
    color: #fff;
    background: none;
  }

  @media(min-width:768px) {
    #wrapper {
      padding-left: 250px;
    }

    #wrapper.toggled {
      padding-left: 0;
    }

    #sidebar-wrapper {
      width: 250px;
    }

    #wrapper.toggled #sidebar-wrapper {
      width: 0;
    }

    #page-content-wrapper {
      padding: 20px;
      position: relative;
    }

    #wrapper.toggled #page-content-wrapper {
      position: relative;
      margin-right: 0;
    }
  }
  </style>
</head>
<body>
  <div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li>
            <p style="color:#fff;">Personal ID: <?php
            //Get User_ID from session
            $user_info=$this->session->userdata('user_info');
            echo $user_info['User_ID'];?></p>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Home tab information it calls @see registered_user_controller::dashboard($tab), located in controllers file,
          *  which redirect to Home tab.
          */
          echo base_url('Registered_user_controller/dashboard/home');?>">Home</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Category: Jewellery tab information it calls @see registered_user_controller::dashboard($tab), located in controllers file,
          *  which redirect to Category: Jewellery tab.
          */
          echo base_url('Registered_user_controller/dashboard/jewellery');?>">Category: Jewellery</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Category: Pet tab information it calls @see registered_user_controller::dashboard($tab), located in controllers file,
          *  which redirect to Category: Pet tab.
          */
          echo base_url('Registered_user_controller/dashboard/pet');?>">Category: Pet</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Category: Tech Devices tab information it calls @see registered_user_controller::dashboard($tab), located in controllers file,
          *  which redirect to Category: Tech Devices tab.
          */
          echo base_url('Registered_user_controller/dashboard/tech_devices');?>">Category: Tech Devices</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Found Item tab information it calls @see registered_user_controller::dashboard($tab), located in controllers file,
          *  which redirect to Found Item tab.
          */
          echo base_url('Registered_user_controller/dashboard/found_item');?>">Found Item</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Request Item tab information it calls @see main_controller::main_dashboard($tab), located in controllers file,
          *  which redirect to Request Item tab.
          */
          echo base_url('Registered_user_controller/dashboard/request_item');?>">Request Item</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To end session Log Out tab information it calls @see main_controller::logout(), located in controllers file,
          *  which redirect to Home [H] tab.
          */
          echo base_url('Registered_user_controller/logout');?>">Log Out</a>
        </li>
      </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <button type="button" name="button"><a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a></button>
          </div>
          <?php
          /**
          * Home tab -> when pressed shows the following information:
          */
            if($tab == 'home'){
              echo "
              <divclass='col-lg-6' style='margin-left:15%; margin-top:2%; width:800px;'>
                <div>
                  <h4 align = 'center'>Welcome to Filo System</h4>
                </div>
                <div>
                  <p align='justify'>
                    <h6>Category:  [nav-bar]</h6>
                      <li> Allow you to see all lost items of jewellery category and their information such as ID, name, colour, location where it found, time, date and descriptions. </li>
                    <br>
                    <h6>Category: Pet [nav-bar]</h6>
                      <li> Allow you to see all lost items of jewellery category and their information such as ID, name, colour, location where it found, time, date and descriptions.  </li>
                    <br>
                    <h6>Category: Tech Devices [nav-bar]</h6>
                        <li> Allow you to see all lost items of tech devices category and their information such as ID, name, colour, location where it found, time, date and descriptions.  </li>
                    <br>
                    <h6>Found Item [nav-bar]</h6>
                        <li>Allow you to add a lost item into the system. Once added you will receive a confermation email.</li>
                    <br>
                    <h6>Request Item [nav-bar]</h6>
                        <li>Allow you to request an item from any category. You must provide a valid  ID of the requested item [ you can obtain it from category section ]. Once submitted the request you will receive a confermation email along your Reques ID if given data are valid. Furthermore, you will receive an email on whether request is approved or refused from Filo System Management.</li>
                    <br>
                    <h6>Log Out [nav-bar]</h6>
                        <li>Allow you to end your session.</li>

                  </p>
                </div>

              </div>

              ";
            }
          ?>

          <?php
          /**
          * Category: Jewellery tab -> when pressed shows all items lost of Jewellery category and their
          * info such as [ Item ID,	Item Name,	Date,	Tim,	Place,	Colour, Image, and	Description ]
          */
          if($tab == 'jewellery') {
            echo "
            <divclass='col-lg-6' style='border:2px solid black; align:center;  margin-left:12%; margin-top:2%;'>
            <h4 align = 'center'>Jewellery Category</h4>
              <div class = 'table-responsive' style='max-height: 400px; max-width: 750px;'>
                <table class='table table-bordered'>
                  <tr>
                    <th>Item ID</th>
                    <th>Item Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Place</th>
                    <th>Colour</th>
                    <th>Description</th>
                  </tr>";

                  // It fetches data from lost_items table from database.
                  // The table only shows lost items that has not been claimed by any user.
                  if($fetch_data->num_rows() > 0 ){
                    foreach ($fetch_data->result() as $row) {
                      if($row->Category_ID == 1 && $row->Claimed== 'no'){
                        echo "<tr>";
                          echo "<td>". $row->Lost_item_ID."</td>";
                          echo "<td>". $row->Item_name."</td>";
                          echo "<td>". $row->Date."</td>";
                          echo "<td>". $row->Time."</td>";
                          echo "<td>". $row->Place."</td>";
                          echo "<td  bgcolor='". $row->Colour."'> </td>";
                          echo "<td>". $row->Description."</td>";
                        echo "</tr>";
                      }
                    }
                  }else{
                    echo "
                      <tr>
                        <td colspan='3'> No Data Found </td>
                      </tr>
                    ";
                  }
                echo "
                </table>
              </div>
            </div>";
          } ?>

          <?php
          /**
          * Category: Pet tab -> when pressed shows all items lost of Jewellery category and their
          * info such as [ Item ID,	Item Name,	Date,	Tim,	Place	Colour, and	Description ]
          */
          if($tab == 'pet') {
            echo "
            <divclass='col-lg-6' style='border:2px solid black; align:center; max-height: 500px; max-width: 750px; margin-left:12%; margin-top:2%;'>
            <h4 align = 'center'>Pet Category</h4>
              <div class = 'table-responsive' style='max-height: 400px; max-width: 750px;'>
                <table class='table table-bordered'>
                  <tr>
                    <th>Item ID</th>
                    <th>Item Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Place</th>
                    <th>Colour</th>
                    <th>Description</th>
                  </tr>";

                  // It fetches data from lost_items table from database.
                  // The table only shows lost items that has not been claimed by any user.
                  if($fetch_data->num_rows() > 0 ){
                    foreach ($fetch_data->result() as $row) {
                      if($row->Category_ID == 2 && $row->Claimed== 'no' ){
                        echo "<tr>";
                          echo "<td>". $row->Lost_item_ID."</td>";
                          echo "<td>". $row->Item_name."</td>";
                          echo "<td>". $row->Date."</td>";
                          echo "<td>". $row->Time."</td>";
                          echo "<td>". $row->Place."</td>";
                          echo "<td  bgcolor='". $row->Colour."'> </td>";
                          echo "<td>". $row->Description."</td>";
                        echo "</tr>";
                      }
                    }
                  }else{
                    echo "
                      <tr>
                        <td colspan='3'> No Data Found </td>
                      </tr>
                    ";
                  }
                echo "
                </table>
              </div>
            </div>";
          } ?>
          <?php
          /**
          * Category: Tech Devices tab -> when pressed shows all items lost of Jewellery category and their
          * info such as [ Item ID,	Item Name,	Date,	Tim,	Place	Colour, and	Description ]
          */
          if($tab == 'tech_devices') {
            echo "
            <divclass='col-lg-6' style='border:2px solid black; align:center; max-height: 500px; max-width: 750px; margin-left:12%; margin-top:2%;'>
              <h4 align = 'center'>Tech Device Category</h4>
              <div class = 'table-responsive' style='max-height: 400px; max-width: 750px;'>
                <table class='table table-bordered'>
                  <tr>
                    <th>Item ID</th>
                    <th>Item Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Place</th>
                    <th>Colour</th>
                    <th>Description</th>
                  </tr>";

                  // It fetches data from lost_items table from database.
                  // The table only shows lost items that has not been claimed by any user.
                  if($fetch_data->num_rows() > 0 ){
                    foreach ($fetch_data->result() as $row) {
                      if($row->Category_ID == 3 && $row->Claimed== 'no'){
                        echo "<tr>";
                          echo "<td>". $row->Lost_item_ID."</td>";
                          echo "<td>". $row->Item_name."</td>";
                          echo "<td>". $row->Date."</td>";
                          echo "<td>". $row->Time."</td>";
                          echo "<td>". $row->Place."</td>";
                          echo "<td  bgcolor='". $row->Colour."'> </td>";
                          echo "<td>". $row->Description."</td>";
                        echo "</tr>";
                      }
                    }
                  }else{
                    echo "
                      <tr>
                        <td colspan='3'> No Data Found </td>
                      </tr>
                    ";
                  }
                echo "
                </table>
              </div>
            </div>";
          } ?>

          <?php
          /**
          * Found Item tab -> allow user to add a lost item into the database. When 'Submit Found Item' button is submitted it calls
          * @see registered_user_controller::found_item_for_validation(), located in controllers folder, which validate data
          * and add them to database.
          *
          */
          if($tab == 'found_item') {
            echo "
            <div class='col-lg-6' style='border:2px solid black; align:center; margin-left:20%; margin-top:2%;'>
              <br>
              <h4><p align='center'>Found Item Form</p></h4>
              <form method='post' action='"; echo base_url('Registered_user_controller/found_item_for_validation');echo "'>";
              echo"
                <div class='form-group'>

                  <label>Choose Item Category: &nbsp &nbsp &nbsp</label>

                  <label class='radio-inline'>
                    <input type='radio' name='Category' value='1'>Jewellery &nbsp&nbsp
                  </label>

                  <label class='radio-inline'>
                    <input type='radio' name='Category' value='2'>Pet &nbsp&nbsp
                  </label>

                  <label class='radio-inline'>
                    <input type='radio' name='Category' value='3'>Tech Device &nbsp&nbsp
                  </label>

                  <span class='text-danger'>"; echo form_error('Found_item_Category');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Enter item name: </label>
                  <input type='text' name='Item_name' id='Item_name'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Item_name');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Enter the date you found the item: </label>
                  <input type='date' name='Found_item_date' id='Found_item_date'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Found_item_date');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Enter the time you found the item: </label>
                  <input type='time' name='Found_item_time' id='Found_item_time'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Found_item_time');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Enter the location where you found the item: </label>
                  <input type='text' name='Found_item_place' id='Found_item_place'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Found_item_place');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Enter item colour: </label>
                  <input type='color' name='Found_item_colour' id='Found_item_colour'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Found_item_colour');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Upload item image: </label><br>
                  <input type='file' name='Found_item_image' id='Found_item_image' accept='image/*'/>
                  <span class='text-danger'>"; echo form_error('Found_item_image');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Enter item description/additional information: </label>
                  <input type='text' name='Found_item_desc' id='Found_item_desc'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Found_item_desc');echo"</span>
                </div>

                <div class='form-group'>
                  <input type='submit' name='insert' value='Submit Found Item' class='btn btn-info'>
                </div>

                <h6 align = 'center'>Once you have sumbitted you will receive an email of confermation. You are only allowed to upload image extensions.</h6>

              </form>
            </div>";
          } ?>

          <?php
          /**
          * Request Item tab -> allow user to request a lost item from any category. When 'Request Item' button is submitted it calls
          * @see registered_user_controller::requested_item_for_validation(), located in controllers folder, which validate data
          * and add the request to database if they met the requirement.
          */
          if($tab == 'request_item') {
            echo "
            <div class='col-lg-6' style='border:2px solid black; align:center; margin-left:20%; margin-top:2%;'>
              <br>
              <h4><p align='center'>Request Item Form</p></h4>
              <form method='post' action='"; echo base_url('Registered_user_controller/requested_item_for_validation');echo "'>

                <div class='form-group'>
                  <label>Requested item ID</label>
                  <input type='number' name='Lost_item_ID' id='Lost_item_ID'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Lost_item_ID');echo"</span>
                </div>

                <div class='form-group'>
                  <label>Enter request reason: </label>
                  <input type='text' name='Request_reason' id='Request_reason'class='form-control' />
                  <span class='text-danger'>"; echo form_error('Request_reason');echo"</span>
                </div>

                <div class='form-group'>
                  <input type='submit' name='insert' value='Request Item' class='btn btn-info'>
                </div>

                <h6 align = 'center'>If Request item ID is valid you will receive an email of confermation. You can get Item ID from category.</h6>

              </form>
            </div>";
          } ?>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->
  <!-- Menu Toggle Script -->
  <script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
  </script>
</body>
</html>
