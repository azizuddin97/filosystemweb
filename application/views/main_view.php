<!DOCTYPE html>
<!-- main_view.php -> main view of Filo System -->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Aziz Uddin">
  <title>Filo System</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <style>
  .bs-example{
    margin-top: 10px;
    margin-left: 20px;
    margin-right: 20px;
  }
  .page-header{
    margin-top: 10px;
  }
  footer {
  margin-bottom: 0px;
  background-color: white;
  padding: 10px;
  text-align: center;
  color: black;
  }
  </style>
</head>
<body>
  <div class="container">
    <div class="page-header" align="center">
      <h1>Filo System</h1>
    </div>
    <div class="bs-example" class="col-lg-12">
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a href="<?php
          /**
          * To show Home [H] tab information it calls @see main_controller::main_dashboard($tab), located in controllers folder,
          *  which redirect to Home [H] tab.
          */
          echo base_url('Main_controller/main_dashboard/home');
          ?>" class="nav-link">Home [H]</a>
        </li>
        <li class="nav-item">
          <a href="<?php
          /**
          * To show Public User [P] tab information it calls @see main_controller::main_dashboard($tab), located in controllers folder,
          * which redirect to Public User [P] tab.
          */
          echo base_url('Main_controller/main_dashboard/public_user');?>" class="nav-link">Public User [P]</a>
        </li>
        <li class="nav-item">
          <a href="<?php
          /**
          * To show Registered User [P] tab information it calls @see main_controller::main_dashboard($tab), located in controllers folder,
          * which redirect to Registered User [P].
          */
          echo base_url('Main_controller/main_dashboard/registered_user');?>" class="nav-link">Registered User [R]</a>
        </li>
        <li class="nav-item">
          <a href="<?php
          /**
          * To show Admin [A] tab information it calls @see main_controller::main_dashboard($tab), located in controllers folder,
          * which redirect to Admin [A] tab.
          */
          echo base_url('Main_controller/main_dashboard/administrator');?>" class="nav-link">Admin [A]</a>
        </li>
        <li class="nav-item">
          <a href="<?php
          /**
          * To show Info [I] tab information it calls @see main_controller::main_dashboard($tab), located in controllers folder,
          * which redirect to Info [I] tab.
          */
          echo base_url('Main_controller/main_dashboard/info');?>" class="nav-link">Info [I]</a>
        </li>
      </ul>
      <div class="tab-content">
        <?php
        /**
        * Home [H] tab -> when pressed shows the following information:
        */
          if($tab == 'home'){
            echo "
            <div class='tab-pane fade show active'>
              <br>
              <p align='justify'>Everyday, hundreds and thousands of valuable items are lost from home, trains, and airports etc.
                Many of those lost items are never returned to their owners because it is just very difficult to link a lost
                item to the owner. You are required to design and implement a new Find-the-Lost website called FiLo
                system. The system could provide a lost item database so that users could check the lost items there
                and could add new items they found into it. To simplify the development of this application, we only
                consider three categories of items: pets, phones and jewellery and only require a partial
                implementation of the Filo system. For the three types of users of the FiLo system, the main functions
                are:<br>
                <h6>For public users [P]:</h6>
                1) View basic item information (e.g. category, color, date) based on the three categories.<br>
                2) Register to become a registered user<br><br>
                <h6>For registered users [R]:</h6>
                1) Log in/out the system<br>
                2) Add a found item by inputting the item details<br>
                3) View basic item information (e.g. category, color, date) based on the three categories and<br>
                click one item to see more details (e.g. photo, place, description etc.).<br>
                4) Request an item and give the reason of the request<br><br>
                <h6>For administrator [A]:</h6>
                1) Log in/out the system<br>
                2) View all the requests<br>
                3) Approve/refuse the requests<br>
                <h6 align='center'><u>Default user login and admin login are on <u>Info [I] tab.</u></h6>
              </p>
            </div>";
          }
        ?>
        <?php
        /**
        * Public User [P] tab -> when pressed shows the user registration form. When registration button is submitted it calls
        * @see main_controller::registration_form_validation(), located in controllers folder, which validate registration data.
        *
        * The registration form takes in input user name, user surname, user email and user password.
        * All these fields are required.
        *
        * Furthermore, Public User [P] tab -> also shows a preview of lost items [item name, item place and item colour] in a table . To fetch data from database it calls
        * @see main_controller::main_dashboard($tab), located in controllers folder
        */
          if($tab == 'public_user'){
            echo "
            <div class='tab-pane fade show active'>
              <br><br>
              <div class='container'>
                <div class='row'>
                  <div class='col-md'>
                    <h6><p align='center'>Registration Form [P]</p></h6>
                    <form method='post' action='"; echo base_url('Main_controller/registration_form_validation'); echo "'>";
                echo "
                    <div class='form-group'>
                      <label>Enter First Name</label>
                      <input type='text' name='User_name' class='form-control' />
                      <span class='text-danger'>"; echo form_error('User_name'); echo"</span>
                    </div>

                    <div class='form-group'>
                      <label>Enter Last Name</label>
                      <input type='text' name='User_surname' class='form-control' />
                      <span class='text-danger'>"; echo form_error('User_surname'); echo"</span>
                    </div>

                    <div class='form-group'>
                      <label>Enter Your Valid Email Address</label>
                      <input type='email' name='User_email' class='form-control' />
                      <span class='text-danger'>"; echo form_error('User_email'); echo"</span>
                    </div>

                    <div class='form-group'>
                      <label>Enter Password</label>
                      <input type='password' name='User_password' class='form-control' />
                      <span class='text-danger'>"; echo form_error('User_password'); echo "</span>
                    </div>

                    <div class='form-group'>
                      <input type='submit' name='insert' value='Register' class='btn btn-info'>
                    </div>
                    <p align='justify' style='font-size:15px;'>If registred succesfully you will receive an Email with all your registration details. In case you won't receive an email consider your registration unsuccessfull.</p>
                  </form>
                </div>

                <div class='col-md'>
                <br><br>
                <h6 align = 'center'>Preview of lost items</h6>
                    <div class = 'table-responsive' style='height: 200px; border: solid black 2px'>
                      <table class='table table-bordered'>
                        <tr>
                          <th>Item Name</th>
                          <th>Place</th>
                          <th>Colour</th>
                        </tr>";

                        //It fetches data from lost_items table from database and gives a preview of lost items information such as name,
                        //place where is been lost, and colour of it. The table only shows lost items that has not been claimed by any user.
                        if($fetch_data_from_ite_lost->num_rows() > 0 ){
                          foreach ($fetch_data_from_ite_lost->result() as $row) {
                            if($row->Claimed== 'no'){
                              echo "<tr>";
                                echo "<td>". $row->Item_name."</td>";
                                echo "<td>". $row->Place."</td>";
                                echo "<td  bgcolor='". $row->Colour."'> </td>";
                              echo "</tr>";
                            }
                          }
                        }else{
                          echo "
                            <tr>
                              <td colspan='3'> No Data Found </td>
                            </tr>
                          ";
                        }
                      echo "

                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>";

          }
        ?>

        <?php
        /**
        * Registered User [R] -> when pressed shows the user login form. When button is submitted it calls
        * @see main_controller::user_login_form_validation(), located in controllers folder, which validate user login data.
        *
        * The user login form takes in input user ID, user email and user password. All these attributes are required.
        */
            if($tab == 'registered_user'){
              echo "
              <div class='tab-pane fade show active'>
                <br><br>
                <div class='container'>
                  <div class='row'>
                    <div class='col-lg-12'>
                      <h6><p align='center'>Login Form</p></h6>
                      <form method='post' action='"; echo base_url('Main_controller/user_login_form_validation'); echo "'>";
                      echo "
                        <div class='form-group'>
                          <label>Enter Your Personal ID</label>
                          <input type='number' name='User_ID' class='form-control' />
                          <span class='text-danger'>"; echo form_error('User_ID'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <label>Enter Your Valid Email Address</label>
                          <input type='email' name='User_email' class='form-control' />
                          <span class='text-danger'>"; echo form_error('User_email'); echo"</span>
                        </div>

                        <div class='form-group'>
                          <label>Enter Password</label>
                          <input type='password' name='User_password' class='form-control' />
                          <span class'text-danger'>";echo form_error('User_password'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <input type='submit' name='insert' value='Login' class='btn btn-info'>
                        </div>

                        <p align='center' style='font-size:15px;''>If logged succesfully you will enter to user main dashboard. </p>

                      </form>
                    </div>
                </div>
              </div>";
            }
        ?>

        <?php
        /**
        * Admin [A] -> when pressed shows the user Admin login form. When button is submitted it calls
        * @see main_controller::admin_login_form_validation(), located in controllers folder, which validate admin login data.
        *
        * The admin login form takes in input admin ID, admin name and admin password. All these attributes are required.
        */
          if($tab == 'administrator'){
            echo "
            <div class='tab-pane fade show active'>
              <br><br>
              <div class='container'>
                <div class='row'>
                  <div class='col-lg-12'>
                    <h6><p align='center'>Admin Login</p></h6>
                    <form method='post' action='"; echo base_url('Main_controller/admin_login_form_validation');echo"'>
                      <div class='form-group'>
                        <label>Enter Admin ID</label>
                        <input type='number' name='Admin_ID' class='form-control' />
                        <span class='text-danger'>";echo form_error("Admin_ID"); echo"</span>
                      </div>

                      <div class='form-group'>
                        <label>Enter Admin Name</label>
                        <input type='text' name='Admin_name' class='form-control' />
                        <span class='text-danger'>"; echo form_error('Admin_name'); echo"</span>
                      </div>

                      <div class='form-group'>
                        <label>Enter Admin Password</label>
                        <input type='password' name='Admin_password' class='form-control' />
                        <span class='text-danger'>"; echo form_error('Admin_password'); echo"</span>
                      </div>

                      <div class='form-group'>
                        <input type='submit' name='insert' value='Login' class='btn btn-info'>
                      </div>

                      <p align='center' style='font-size:15px;''>If logged succesfully you will enter to admin main dashboard. </p>

                    </form>
                  </div>
                </div>
              </div>
            </div>
            ";
          }
        ?>

        <?php
        /**
        * Info [I] tab -> when pressed shows the information such as defaul user login, default admin login,
        * technologies used for the website,  references and author of the web site.
        */
          if($tab == 'info'){
            echo "
            <div class='tab-pane fade show active'>
                <div>
                  <p align='justify'>
                    <h6>Default User Login: </h6>
                      <li>Personal ID: 1</li>
                      <li>Email Address: filosystemuser@gmail.com</li>
                      <li>Password: FSUser_1</li>
                    <br>
                    <h6>Admin Default Login</h6>
                      <li>Admin ID: 1</li>
                      <li>Admin Name: Marco</li>
                      <li>Password: FiloSystemAdmin</li>
                    <br>
                    <h6>Technologies used to develop the website: </h6>
                      <li>PHP</li>
                      <li>MySQL</li>
                      <li>HTML</li>
                      <li>CSS</li>
                      <li>Bootstrap</li>
                      <li>Web Framework: Codeigniter 4.0.0</li>
                    <br>
                    <footer>
                      <p>Developed by Aziz Uddin contact to: <a href='mailto:Uddin.az123@gmail.com'>Uddin.az123@gmail.com</a></p>
                    </footer>
                  </p>
                </div>

            </div>";
          }
        ?>

    </div>
  </div>
</body>
</html>
