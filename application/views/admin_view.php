<!DOCTYPE html>
<!-- admi_view.php -> main view of Filo System -->
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="author" content="Aziz Uddin">
  <title>Registered User</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <style>
  #wrapper {
    padding-left: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  }

  #wrapper.toggled {
    padding-left: 250px;
  }

  #sidebar-wrapper {
    z-index: 1000;
    position: fixed;
    left: 250px;
    width: 0;
    height: 100%;
    margin-left: -250px;
    overflow-y: auto;
    background: #000;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  }

  #wrapper.toggled #sidebar-wrapper {
    width: 250px;
  }

  #page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 15px;
  }

  #wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -250px;
  }

  /* Sidebar Styles */

  .sidebar-nav {
    position: absolute;
    top: 0;
    width: 250px;
    margin: 0;
    padding: 0;
    list-style: none;
  }

  .sidebar-nav li {
    text-indent: 20px;
    line-height: 40px;
  }

  .sidebar-nav li a {
    display: block;
    text-decoration: none;
    color: #999999;
  }

  .sidebar-nav li a:hover {
    text-decoration: none;
    color: #fff;
    background: rgba(255,255,255,0.2);
  }

  .sidebar-nav li a:active,
  .sidebar-nav li a:focus {
    text-decoration: none;
  }

  .sidebar-nav > .sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
  }

  .sidebar-nav > .sidebar-brand a {
    color: #999999;
  }

  .sidebar-nav > .sidebar-brand a:hover {
    color: #fff;
    background: none;
  }

  @media(min-width:768px) {
    #wrapper {
      padding-left: 250px;
    }

    #wrapper.toggled {
      padding-left: 0;
    }

    #sidebar-wrapper {
      width: 250px;
    }

    #wrapper.toggled #sidebar-wrapper {
      width: 0;
    }

    #page-content-wrapper {
      padding: 20px;
      position: relative;
    }

    #wrapper.toggled #page-content-wrapper {
      position: relative;
      margin-right: 0;
    }
  }
  </style>
</head>
<body>
  <div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li>
            <p style="color:#fff;">Admin ID: <?php
            $admin_info=$this->session->userdata('admin_info');
            echo $admin_info['Admin_ID'];?></p>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Home tab information it calls @see admin_controller::admin_dashboard($tab), located in controllers file,
          *  which redirect to Home tab.
          */
          echo base_url('Admin_controller/admin_dashboard/home');?>">Home</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To show User Request tab information it calls @see admin_controller::admin_dashboard($tab), located in controllers file,
          *  which redirect to User Request tab.
          */
          echo base_url('Admin_controller/admin_dashboard/user_requests');?>">User Requests</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To show Users Info tab information it calls @see admin_controller::admin_dashboard($tab), located in controllers file,
          *  which redirect to Users Info tab.
          */
          echo base_url('Admin_controller/admin_dashboard/user_info');?>">Users Info</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To showLost Items tab information it calls @see admin_controller::admin_dashboard($tab), located in controllers file,
          *  which redirect to Lost Items tab.
          */
          echo base_url('Admin_controller/admin_dashboard/lost_items');?>">Lost Items</a>
        </li>
        <li>
          <a href="<?php
          /**
          * To end session Log Out tab information it calls @see admin_controller::logout(), located in controllers file,
          *  which redirect to Home [H] tab.
          */
          echo base_url('Admin_controller/logout');?>">Log Out</a>
        </li>
      </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <button type="button" name="button"><a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a></button>
          </div>
          <?php
          /**
          * Home tab -> when pressed shows the following information:
          */
          if($tab == 'home'){
              echo "
              <divclass='col-lg-6' style='margin-left:15%; margin-top:2%; width:800px;'>
                <div>
                  <h4 align = 'center'>Welcome to Filo System [Admin]</h4>
                </div>
                <div>
                  <p align='justify'>
                    <h6>User Requests [nav-bar]</h6>
                      <li> Allow you to visualize all the requests [ Request ID, User ID, User Surname, Lost Item ID, and Request Reason ] made by users and to approve or refuse the latter. Once a decision is made user will be notified through email.</li>
                    <br>
                    <h6>User Info [nav-bar]</h6>
                        <li> Allow you to visualize all user private information [ User ID, User Name, User Surname, User Email, and User Password ].</li>
                    <br>
                    <h6>Lost Items [nav-bar]</h6>
                        <li>Allow you to visualize all lost items and to edit their attributes [ Item Name, Item Place and Item Colour ]. Once edit is made Lost Item table is updated.</li>
                    <br>
                    <h6>Log Out [nav-bar]</h6>
                        <li>Allow you to end your session.</li>
                  </p>
                </div>

              </div>

              ";
            }
          ?>

          <?php
          /**
          * User Request tab -> allow Admin to see all the requests made by users and to to approve or refuse them. When 'Confirm'
          * button is pressed it calls @see admin_controller::approve_or_refuse_requests(), located in controllers folder, which validate admid data
          * such as Reques ID and decision [Approve and Refuse]
          *
          * Once approved or refused Request table is approved after that request verdict cannot be changed anymore.
          */
          if($tab == 'user_requests') {
            echo "
            <div class='tab-pane fade show active'>

              <br><br>
              <div class='container'>

                <div class='row'>

                    <div class='col-md' style='margin-top: 5%;;'>

                      <h6><p align='center'>Approve/Refuse Requests</p></h6>
                      <form method='post' action='"; echo base_url('Admin_controller/approve_or_refuse_requests'); echo "'>";
                      echo "
                        <div class='form-group'>
                          <label>Enter Request ID</label>
                          <input type='number' name='Request_ID' class='form-control' />
                          <span class='text-danger'>"; echo form_error('Request_ID'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <label for='Admin_verdict'>Choose a car:</label>
                          <select id='Admin_verdict' name='Admin_verdict' class='form-control'>
                            <option value='Approve'>Approve</option>
                            <option value='Refuse'>Refuse</option>
                          </select>
                          <span class='text-danger'>"; echo form_error('Admin_verdict'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <input type='submit' name='inseret' value='Confirm' class='btn btn-info'>
                        </div>

                        <p align='justify' style='font-size:15px;''>When confirmed user will be notified of your decision and List of Requests table will be updated.</p>

                      </form>

                    </div>

                    <div class='col-md' style='max-height: 100px; '>
                      <h6 align = 'center'>List of Requests</h6>

                      <div class = 'table-responsive' style = 'max-height: 400px; max-width: 800px; border: solid black 2px;'>
                        <table class='table table-bordered'>
                          <tr>
                            <th>Request ID</th>
                            <th>User ID</th>
                            <th>User Surname</th>
                            <th>Lost Item ID</th>
                            <th>Request Reason</th>
                            <th>Request Approval</th>
                          </tr>";
                            //Fetch all requests from request_item  table. It shows all user requests.
                            if($fetch_data_user_requests->num_rows() > 0 ){
                              foreach ($fetch_data_user_requests->result() as $row) {
                                  echo "<tr>";
                                    echo "<td>". $row->Request_ID."</td>";
                                    echo "<td>". $row->User_ID."</td>";
                                    echo "<td>". $row->User_surname."</td>";
                                    echo "<td>". $row->Lost_item_ID."</td>";
                                    echo "<td>". $row->Request_reason."</td>";
                                    echo "<td>". $row->Request_approval."</td>";
                                  echo "</tr>";
                                }
                              }else{
                                echo "<tr>
                                        <td colspan='3'> No Data Found </td>
                                     </tr>";
                              }
                echo "  </table>
                    </div>
                  </div>

              </div>
            </div>";
          } ?>

          <?php
          /**
          * User Info tab -> Shows all user info such as User ID, User name, User Surname, User Email and User Password,
          * in a table.
          */
          if($tab == 'user_info') {
            echo "
            <divclass='col-lg-6' style='align:center; margin-left:10%; margin-right:10%; '>
              <h4 align = 'center'>User Information</h4>
              <div class = 'table-responsive'>
                <table class='table table-bordered' style='border:2px solid black; max-height:20px; '>
                  <tr>
                    <th>User ID</th>
                    <th>User Name</th>
                    <th>User Surname</th>
                    <th>User Email</th>
                    <th>User Password</th>
                  </tr>";
                  //Fetches user data and creates a table out of it.
                  if($fetch_user_info->num_rows() > 0 ){
                    foreach ($fetch_user_info->result() as $row) {
                        echo "<tr>";
                          echo "<td>". $row->User_ID."</td>";
                          echo "<td>". $row->User_name."</td>";
                          echo "<td>". $row->User_surname."</td>";
                          echo "<td>". $row->User_email."</td>";
                          echo "<td>". $row->User_password."</td>";
                        echo "</tr>";

                    }
                  }
                  else{
                    echo "
                      <tr>
                        <td colspan='3'> No Data Found </td>
                      </tr>
                    ";
                  }
                echo "
                </table>
              </div>
            </div>";
          } ?>

          <?php
          /**
          * Lost item tab -> allow Admin to see all lost items [ Item ID,	Item Name	Date,	Time,	Place,	Colour,	Claimed, and	Description ], their details and whether they have been claimed or not.
          * In addition it allows to edit [Item name, Item Colour and Item description].
          *
          * Once the 'Edit' button is pressed it calls @see admin_controller::update_lost_item_table() which validate admin editing info and update Lost Item table.
          *
          * Admin can choose to edit any field ot of [Item name, Item Colour and Item description], but before editing admin must choose at least one field to edit.
          */
          if($tab == 'lost_items') {
            echo "
            <div class='tab-pane fade show active'>
              <br><br>
              <div class='container'>
                <div class='row'>
                    <div class='col-md' style='margin-top: 0%;'>
                      <h6><p align='center'>Edit/Update Lost Item Data</p></h6>
                      <form method='post' action='"; echo base_url('Admin_controller/update_lost_item_table'); echo "'>";
                      echo "
                        <div class='form-group'>
                          <label>Enter Item ID</label>
                          <input type='number' name='Lost_item_ID' class='form-control' />
                          <span class='text-danger'>"; echo form_error('Lost_item_ID'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <label>Edit Item Name</label>
                          <input type='text' name='Item_name' class='form-control' />
                          <span class='text-danger'>"; echo form_error('Item_name'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <label>Edit Place</label>
                          <input type='text' name='Place' class='form-control' />
                          <span class='text-danger'>"; echo form_error('Place'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <label>Edit Description</label>
                          <input type='text' name='Description' class='form-control' />
                          <span class='text-danger'>"; echo form_error('Description'); echo "</span>
                        </div>

                        <div class='form-group'>
                          <input type='submit' name='inseret' value='Edit' class='btn btn-info'>
                        </div>

                        <p style='font-size:15px;'>When confirmed Lost Item Table will be updated.</p>

                      </form>
                    </div>
                    <div class='col-md'>
                      <h6 align = 'center'>Lost Item Table</h6>
                      <div class = 'table-responsive' style = 'margin-left: 30px; margin-right: 0px; max-height: 400px; max-width: 600px; border: solid black 2px;'>
                        <table class='table table-bordered'>
                          <tr>
                            <th>Item ID</th>
                            <th>Item Name</th>
                            <th>Item Category</th>
                            <th>User ID</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Place</th>
                            <th>Colour</th>
                            <th>Claimed</th>
                            <th>Description</th>
                          </tr>";
                            //Fetch lost items from databse and creates a table out of it.
                            if($fetch_lost_item_info->num_rows() > 0 ){
                              foreach ($fetch_lost_item_info->result() as $row) {
                                echo "<tr>";
                                  echo "<td>". $row->Lost_item_ID."</td>";
                                  echo "<td>". $row->Item_name."</td>";
                                  echo "<td>". $row->Category_ID."</td>";
                                  echo "<td>". $row->User_ID."</td>";
                                  echo "<td>". $row->Date."</td>";
                                  echo "<td>". $row->Time."</td>";
                                  echo "<td>". $row->Place."</td>";
                                  echo "<td  bgcolor='". $row->Colour."'> </td>";
                                  echo "<td>". $row->Claimed."</td>";
                                  echo "<td>". $row->Description."</td>";
                                echo "</tr>";
                                }
                              }else{
                                echo "<tr>
                                        <td colspan='3'> No Data Found </td>
                                     </tr>";
                              }
                echo "  </table>
                      </div>
                    </div>
              </div>
            </div>";
          } ?>

        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->
  <!-- Menu Toggle Script -->
  <script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
  </script>
</body>
