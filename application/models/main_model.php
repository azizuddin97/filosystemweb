<?php
/**
* Main_model fetch, stores and manipulate data from databse.
*/
class Main_model extends CI_Model{
  /**
  * This method takes @param data, an array of data [user name, user surname, user email and user password]
  * and stores them to database registered_user table.
  *
  * @return User_ID, unique User ID [Primary Key] which later is used by user as one of the login attribute.
  */
  function register_data($data){
    //Insert array data to registered_user table
    $this->db->insert('Registered_user',$data);
    //return row number whihc is also a Primary Key
    return $this->db->insert_id();
  }

  /**
  * This method takes @param data, array of data [user ID, user email, admin password]
  * and checkes whether they are valid or not.
  * @return boolean true if data matches, false if data dont matches.
  */
  function user_login_data($data){
    //Get row where User_ID == $data['User_ID']
    $this->db->where('User_ID',$data['User_ID']);
    //Get User_ID from registered_user table
    $query = $this->db->get('Registered_user');
    if($query->num_rows() > 0){
      foreach($query->result() as $row){
        //Go through each row and see if given data match.
        if($row->User_email == $data['User_email'] && $row->User_password == $data['User_password']&& $row->User_ID == $data['User_ID']){
          return true;
        }
      }
    }else{
      return false;
    }
  }
  /**
  * This method takes @param data, array of data [admin ID, admin name, admin password]
  * and checkes whether they are valid or not.
  * @return boolean true if data matches, false if data dont matches.
  */
  function admin_login_data($data){
    //Get row where Admin_name == $data['Admin_name']
    $this->db->where('Admin_ID',$data['Admin_ID']);
    //Get Admin_ID from admin table
    $query = $this->db->get('Admin');
    if($query->num_rows() > 0){
      foreach($query->result() as $row){
        if($row->Admin_name == $data['Admin_name'] && $row->Admin_password == $data['Admin_password']){
          return true;
        }
      }
    }else{
      return false;
    }
  }
  /**
  * This method fetch data from lost_items table.
  * @return query, whihc is whole lost_items table.
  */
  function fetch_data_from_lost_item_table(){
    //Get lost_items table from database
    $query = $this->db->get('Lost_items');
    return $query;
  }
}
?>
