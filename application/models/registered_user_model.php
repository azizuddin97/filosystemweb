<?php
/**
* Registered_user_model class fetches, stores and manipulate data from database.
*/
class Registered_user_model extends CI_Model{
  /**
  * This method takes @param data, an array of data [Item Name,	Category ID, User ID, Item Name, Date,	Tim,	Place,	Colour, Image,  and	Description ]
  * and stores them to database lost_item table.
  *
  * @return Lost_item_ID, unique Item ID [Primary Key] which later is used by user for requests.
  */
  function insert_found_item($data){
    ///Insert array data to lost_item table
    $this->db->insert('Lost_items',$data);
    //return row number whihc is also a Primary Key
    return $this->db->insert_id();
  }
  /**
  * This method takes @param data, an array of data [User ID, User surname, Lost_item_ID (aka Item ID), and Request Reason]
  * and stores them to database lost_item table.
  *
  * @return Request_ID, unique Request ID [Primary Key].
  */
  function insert_requested_item($data){
    //Gets lost_items table from database.
    $query = $this->db->get('Lost_items');
    foreach($query->result() as $row)
    {
      //checks that the given Lost_item_ID matches with the one in database.
      if($row->Lost_item_ID == $data['Lost_item_ID']){
        //When matched add array $data to request_item table in database.
        $this->db->insert('Request_item',$data);
        //returns row ID whihc is also a PRIMARY KEY.
        return $this->db->insert_id();
      }
    }
    //If Lost_item_ID dont match send back -1.
    return -1;
  }
  /**
  * Fetch lost_items table from datbase.
  * @return query, which is lost_items table.
  */
  function fetch_data(){
    //Fetch table.
    $query = $this->db->get('Lost_items');
    //return query.
    return $query;
  }
  /**
  * This method fetch user data from registered_user table.
  * @return temp_user_info, which is an arry containing [user id, user surname, user name and user email].
  */
  function get_user_info(){
    //Gets user info [user id and user email] from session.
    $data = $this->session->userdata('user_info');
    //Fetch table registered_user from database.
    $query = $this->db->get('Registered_user');
    foreach($query->result() as $row)
    {
      //Check that session User email and ID match with the one from database.
      if($row->User_email == $data['User_email'] && $row->User_ID == $data['User_ID'])
      {
        //If matched create an array of user info such as[user id, user surname, user name and user email].
        $temp_user_info = array(
          "User_ID" =>  $row->User_ID,
          "User_surname" => $row->User_surname,
          "User_name" => $row->User_name,
          "User_email" => $row->User_email,
        );
        //Return array.
        return $temp_user_info;
      }
    }
    //If data dont match return -1.
    return -1;
  }
}
?>
