<?php
/**
* Admin_model class fetches, stores and manipulate data from database.
*/
class Admin_model extends CI_Model{
  /**
  * Fetch data from datbase given table name @param table_name.
  * @return query, which is data table.
  */
  public function fetch_data($table_name){
    //Fetch table.
    $query = $this->db->get($table_name);
    //return query.
    return $query;
  }

  /**
  * The following method update Request item table based on admin decision.
  * @param data, an array containig Request ID and Request Approval
  *
  * It also updates Lost Item Table. When Request is approved it changes 'Claimed'
  * attribute from Lost Item Table to 'yes'.
  *
  * @return User_ID, unique Request ID [Primary Key].
  * @return false when User_ID not found on table.
  */
  public function request_decision($data){
    //Get Request ID from database where admin Request -> $data['Request_ID'] match.
    $this->db->where('Request_ID',$data['Request_ID']);
    //Get request_item table from database.
    $query = $this->db->get('Request_item');
    if($query->num_rows() > 0){
      foreach($query->result() as $row){
        //If admin Request_approval is different than 'no'.
        if($row->Request_ID == $data['Request_ID'] && $row->Request_approval != 'no'){
          //Store temporary user apporoval decision.
          $to_be_updated['Request_approval'] = $data['Request_approval'];
          //Check again where Request ID from data matches with admin $data['Request_ID']
          $this->db->where('Request_ID',$data['Request_ID']);
          //update Request_approval attribute from database with admin approval -> $to_be_updated['Request_approval']
          $this->db->update('Request_item', $to_be_updated);
          //This update Lost Item Table
          $this->update_item_lost_table_when_request_approved_or_refused($row->Lost_item_ID);
          //return User ID
          return $row->User_ID;
        }
      }
    }else{
      //If query dont match return false.
      return false;
    }
  }

  /**
  * This method gets user email given @param User_ID [User ID]
  * @return User_email if found in registered_user table.
  * @return false if query not found.
  */
  public function fetch_user_email($user_id){
    //Get User ID from database
    $this->db->where('User_ID',$user_id);
    //From registered_user table
    $query = $this->db->get('Registered_user');

    if($query->num_rows() > 0){
      foreach($query->result() as $row){
        //If given user_id matches with the one in database.
        if($row->User_ID == $user_id){
          //return User_email.
          return $row->User_email;
        }
      }
    }else{
      //if query dont match return false.
      return false;
    }
  }
  /**
  * This method updates Claimed attribute of Lost_item_table in databse.
  * @param Lost_item_ID item ID to be updated
  */
  private function update_item_lost_table_when_request_approved_or_refused($Lost_item_ID){
    //array containing attrribute Claimed value to change into, in this case 'yes'.
    $update_clamed['Claimed'] = 'yes';
    //Get where $Lost_item_ID [given as parameter] equal to $Lost_item_ID in database.
    $this->db->where('Lost_item_ID',$Lost_item_ID);
    //Update lost_item table with new value.
    $this->db->update('Lost_items', $update_clamed);
  }

  /**
  * The following method updates lost_item table when admin edit its ino such as
  * Item_name, Place and Description.
  * @param data an array containg the info that admin wants to be edited.
  */
  public function update_lost_item_table($data){
    //checks if Item_name, Place and Description are empty.
    if(empty($data['Item_name']) && empty($data['Place']) && empty($data['Description'])){
      return false;
    }else{

      //if Item_name not empty store to new array $update
      if (!empty($data['Item_name'])) {
        $update['Item_name'] = $data['Item_name'];
      }
      //if Place not empty store to new array $update
      if (!empty($data['Place'])) {
        $update['Place'] = $data['Place'];
      }
      //if Description not empty store to new array $update
      if (!empty($data['Description'])) {
        $update['Description'] = $data['Description'];
      }
      //Update table where Lost_item_ID from database matches with given $data['Lost_item_ID'].
      $this->db->where('Lost_item_ID',$data['Lost_item_ID']);
      //Update table with new values.
      $this->db->update('Lost_items', $update);
    }
  }


}
?>
