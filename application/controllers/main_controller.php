<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Main_controller class redirect user to the chosen tab, controls,
* validate user registartion details, login info and admin login.
*/
class Main_controller extends CI_Controller {

	/**
	* Mian_controller constructor.
	*/
	function __construct(){
		// Parent constructor @see CI_Controller.
		parent::__construct();
		// If user is logged the following direct to @see registered_user_controller::index(), located in controller folder,
		// and make sure that user seession is not over untill log out tab is pressed from registered_user_view.php,
		// located in views folder.
		if($this->session->userdata('user_info')){
			redirect('Registered_user_controller');
		}
	}

	/**
	* This method show the Home [H] tab when user views the website.
	*/
	public function index()
	{
		//call @see main_controller::main_dashboard('home') which direct user to Home [H] tab.
		$this->main_dashboard('home');
	}

	/**
	* This method validate Public User [P] registration data such as name, surname, email and password.
	* If registration is successfull it sents a confermation email to user with all its
	* registration detail along an unique key made by database.
	*
	* If registration is unsuccessfull it redirect user to Public User [P] tab.
	*/
	public function registration_form_validation(){

		//It load form_validation library.
		$this->load->library('form_validation');

		// The following sets validation rules form user name, user surname, user email and user Password.
		// All these are required to be registered, in case they are missed user will be notified through
		// error message whihc will appear underneath text bar.
		$this->form_validation->set_rules('User_name','First Name', 'required|max_length[30]');
		$this->form_validation->set_rules('User_surname','Last Name', 'required|max_length[30]');
		$this->form_validation->set_rules('User_email','Email', 'required|trim|max_length[40]');
		$this->form_validation->set_rules('User_password','Password', 'required|max_length[20]');

		//If given data met the set rules, run form validation.
		if($this->form_validation->run()){
			//This load @see main_model class, located in models folder.
			$this->load->model('Main_model');
			//Creates an array with user info such as User name, use surname, user email and user password.
			$user = array(
				'User_name'  =>$this->input->post("User_name"),
				'User_surname'  =>$this->input->post("User_surname"),
				'User_email'  =>$this->input->post("User_email"),
				'User_password'  =>$this->input->post("User_password"),
			);
			// $this->main_model->register_data($user) calls method @see main_model::register_data($data),
			// it takes user's array of data, stores to database and returns an unique ID for user which is
			// saved in temporary variable $inserted.
			$inserted = $this->Main_model->register_data($user);
			//User ID is > 0
			if($inserted > 0)
			{
				//Email subject
				$subject = "Filo System: Registration Details";
				//Email message composed of User Unique ID, User name, User surname, User email and User Password.
				$message = "Hi ".$this->input->post('User_name')." the following are your registration details, keep them safely.\n\nPersonal ID: ".$inserted."\nName: ".$this->input->post('User_name')." ".$this->input->post('User_surname')."\nEmail: ".$this->input->post('User_email')."\nPassword: ".$this->input->post('User_password')."\n\nKind Regards,\nFilo System Management";

				//It loads email library.
				$this->load->library('email');
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'ssl://smtp.gmail.com';
				$config['smtp_port']    = '465';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'FiloSystem2020@gmail.com';
				$config['smtp_pass']    = 'AstonUniFilo';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$config['mailtype'] = 'text'; // or html
				$config['validation'] = TRUE; // bool whether to validate email or not

				//Initialize email configuration
				$this->email->initialize($config);

				//Send email from FiloSystem2020@gmail.com
				$this->email->from('FiloSystem2020@gmail.com', 'Admin');
				//Send to:
				$this->email->to($user['User_email']);
				//Email subject:
				$this->email->subject($subject);
				//Email message:
				$this->email->message($message);
				//Send Email
				$this->email->send();


				//Refresh web page and make sure user data are not stored twice or more on database.
				header("Location: " . $_SERVER['REQUEST_URI']);
			}else{
				//If unique ID is < 0 redirect to Public User [P] tab.
				$this->main_dashboard('public_user');
			}
		}else{
			//If user data dont met set rules redirect to Public User [P] tab.
			$this->main_dashboard('public_user');
		}
	}

	/**
	* This method validate Registered User [P] login data such as user ID, user email and user password.
	* If login  is successfull it will be directed to @see registered_user_controller::index(), located in controller folder.
	*
	* If registration is unsuccessfull it will redirect user to Registered User [P] tab.
	*/
	public function user_login_form_validation(){

		//It load form_validation library.
		$this->load->library('form_validation');
		// The following sets validation rules form user ID, user Password, and user email.
		// All these are required to be registered, in case they are missed user will be notified through
		// error message whihc will appear underneath text bar.
		$this->form_validation->set_rules('User_ID','ID', 'required|trim|max_length[5]');
		$this->form_validation->set_rules('User_password','Password', 'required|trim|max_length[20]');
		$this->form_validation->set_rules('User_email','Email', 'required|trim|max_length[40]');

		//If given data met the set rules, run form validation.
		if($this->form_validation->run()){
			//This load @see main_model class, located in models folder.
			$this->load->model('Main_model');
			//Creates an array with user info such as User ID, use email, user email and user password.
			$user= array(
				'User_ID'  =>$this->input->post("User_ID"),
				'User_email'  =>$this->input->post("User_email"),
				'User_password'  =>$this->input->post("User_password"),
			);

			// $this->main_model->user_login_data($user) calls method @see main_model::user_login_data($data),
			// it takes user's array of data and if user info matches with the info from datbase. If data matches
			// return a boolean and it is saved in temporary variable $logged.
			$logged = $this->Main_model->user_login_data($user);

			//if logind data matches
			if($logged == true){
				//This stores user info such as user ID, user email and user password for the session
				$this->session->set_userdata('user_info', $user);
				//direct user to @see registered_user_controller::index().
				redirect('Registered_user_controller');
			}else{
				//If login data dont match with database info redirect to Registered User [P].
				$this->main_dashboard('registered_user');
			}
		}else {
			//If user data dont met set rules redirect to Public User [P] tab.
			$this->main_dashboard('registered_user');
		}
	}

	/**
	* This method validate Admin [A] login data such as admin ID, admin name, and admin password.
	* If login is successfull it will direct to @see admin_controller::index(), located in controller folder.
	*
	* If registration is unsuccessfull it redirect user to Admin[A] tab.
	*/
	public function admin_login_form_validation(){

		//It load form_validation library.
		$this->load->library('form_validation');
		// The following sets validation rules form admin ID, admin name, and user password.
		// All these are required to login, in case they are missed admin will be notified through
		// error message whihc will appear underneath text bar.
		$this->form_validation->set_rules('Admin_ID','ID', 'required|trim|max_length[11]');
		$this->form_validation->set_rules('Admin_name','Name', 'required|trim|max_length[20]');
		$this->form_validation->set_rules('Admin_password','Password', 'required|trim|max_length[20]');

		//If given data met the set rules, run form validation.
		if($this->form_validation->run()){
			//This load @see main_model class, located in models folder.
			$this->load->model('Main_model');
			//Creates an array with admin info such as admin ID, admin name, and admin password.
			$admin_info= array(
				'Admin_ID'  =>$this->input->post("Admin_ID"),
				'Admin_name'  =>$this->input->post("Admin_name"),
				'Admin_password'  =>$this->input->post("Admin_password"),
			);

			// $this->main_model->admin_login_data($admin_info) calls method @see main_model::admin_login_data($data),
			// it takes admins's array of data and if admin info matches with the info from datbase. If data matches
			// return a boolean and it is saved in temporary variable $logged.
			$logged = $this->Main_model->admin_login_data($admin_info);

			//if logind data matches
			if($logged == true){
				//This stores user info such as admin ID, admin name and admin password for the session.
				$this->session->set_userdata('admin_info', $admin_info);
				//Direct admin to @see admin_controller::index().
				redirect('Admin_controller');
			}else{
				//If login data dont match with database info redirect to Admin [A].
				$this->main_dashboard('administrator');
			}
		}else {
			//If Admin data dont met set rules redirect to Admin [A] tab.
			$this->main_dashboard('administrator');
		}
	}
	/**
	* This method direct user to the selected tab.
	* @param $tab is tab name.
	*/
	public function main_dashboard($tab){
		//define tab to an array $data
		$data['tab'] = $tab;
		//This load @see main_model class, located in models folder.
		$this->load->model('Main_model');
		//fetch lost_items data from database and store to $data array. @see main_model::fetch_data_from_lost_item_table().
		$data['fetch_data_from_ite_lost'] = $this->Main_model->fetch_data_from_lost_item_table();
		//load main_view page, located in view folder, and in takes @data as parameter.
		$this->load->view('Main_view', $data);
	}
}
