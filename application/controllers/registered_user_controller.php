<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Registered_user_controller class redirect user to the chosen tab, controls,
* validate user Found Item details, Request Item details, send email, manipulate
* data from database and close session when user log off.
*/
class Registered_user_controller extends CI_Controller{

  /**
  * Registered_user_controller constructor.
  */
  function __construct(){
    // Parent constructor @see CI_Controller.
    parent::__construct();
    // If user is logged off the following ridirect user to
    // @see main_controller::index().
    if(!$this->session->userdata('user_info')){
      redirect(' ', 'refresh');
    }
  }

  /**
  * This method show the Home tab of registered_user_view.php web page.
  */
  function index()
  {
    $this->dashboard('home');
  }

  /**
  * This method validate Found Item tab data such as Category ID, Item name
  * Date, Time, Place, Colour, Photo, and Description.
  *
  * If registration Found Item is successfull it will sent a confermation email to user and
  * lost item table will be updated.
  *
  * If registration is unsuccessfull it will redirect to Found Item tab.
  */
  public function found_item_for_validation(){
    //It load form_validation library.
    $this->load->library('form_validation');

    // The following sets validation rules form Category ID, Item name
    // Date, Time, Place, Colour, Photo, and Description..
    // All these are required apart from image to add and item, in case they are missed
    //user will be notified through error message whihc will appear underneath text bar.
    $this->form_validation->set_rules('Category','Category', 'required');
    $this->form_validation->set_rules('Item_name','Item Name', 'required|trim|max_length[40]');
    $this->form_validation->set_rules('Found_item_time','Time', 'required|trim');
    $this->form_validation->set_rules('Found_item_place','Location', 'required|trim|max_length[40]');
    $this->form_validation->set_rules('Found_item_colour','Colour', 'required|trim|max_length[20]');
    $this->form_validation->set_rules('Found_item_image','Image', '');
    $this->form_validation->set_rules('Found_item_desc','Description', 'required|trimmax_length[500]');

    //This retrive user info such user name, user email, and user ID from session
    $user_info = $this->session->userdata('user_info');

    //If given data met the set rules, run form validation.
    if($this->form_validation->run()){
      //This load @see registered_user_model class, located in models folder.
      $this->load->model('Registered_user_model');

      //Creates an array of Item info [Category ID, Item name, Date, Time, Place, Colour, Photo, and Description]
      $lost_item= array(
        'Category_ID'  =>$this->input->post("Category"),
        'User_ID'  =>$user_info['User_ID'],
        'Item_name' =>$this->input->post("Item_name"),
        'Date'  =>$this->input->post("Found_item_date"),
        'Time'  =>$this->input->post("Found_item_time"),
        'Place'  =>$this->input->post("Found_item_place"),
        'Colour'  =>$this->input->post("Found_item_colour"),
        'Photo'  =>$this->input->post("Found_item_image"),
        'Description'  =>$this->input->post("Found_item_desc"),
      );
      // $this->registered_user_model->insert_found_item($lost_item) calls method @see registered_user_model::insert_found_item($data),
      // it takes user's array of data and if Item info are valid it adds to database [lost_items table]
      $inserted=$this->Registered_user_model->insert_found_item($lost_item);
      //if true
      if($inserted){
        //Creates an array with user info such as user ID, user email and user message.
        $data = array(
          'User_ID' => $user_info['User_ID'],//user ID
          'User_email' => $user_info['User_email'], //user email
          'Item_message' => "item have been received, thank you.", //message to be sent to user email
          'Item' => false,
        );
        //call send_email($data) to send email confermation to user.
        $this->send_email($data);
        //Refresh web page and make sure user data are not stored twice or more on database.
        header("Location: " . $_SERVER['REQUEST_URI']);
      }else{
        // if $inserted == flase, whihc means Found Item has not been added redirect to
        // Found Item tab.
        $this->dashboard('found_item');
      }
    }
    //If user data dont met set rules redirect to Found Item tab
    $this->dashboard('found_item');
  }

  /**
  * This method validate Request Item tab data such as Lost Item ID and Request reason.
  *
  * If Request Item is successfull it will sent a confermation email to user along
  * an unique Request ID.
  *
  * If registration is unsuccessfull it will redirect to Request Item tab.
  */
  public function requested_item_for_validation(){
    //It load form_validation library.
    $this->load->library('form_validation');
    // The following sets validation rules form user Lost Item ID and Request reason.
    // All these are required to be registered, in case they are missed user will be notified through
    // error message whihc will appear underneath text bar.
    $this->form_validation->set_rules('Lost_item_ID','Request item ID', 'required|trim|max_length[11]');
    $this->form_validation->set_rules('Request_reason','Request reason', 'required|trim|max_length[120]');


    //If given data met the set rules, run form validation.
    if($this->form_validation->run()){
      //This load @see registered_user_model class, located in models folder.
      $this->load->model('Registered_user_model');
      // @see registered_user_model::get_user_info()  get user data such as User surname, User ID and user email
      // and save them to array $data.
      $data = $this->Registered_user_model->get_user_info();
      //if $data different than -1.
      if($data != -1){
        //Creates an array of info such as User ID, User surname, Lost Item ID and Request reason
        $request_item= array(
          'User_ID' => $data['User_ID'],
          'User_surname' => $data['User_surname'],
          'Lost_item_ID'  =>$this->input->post("Lost_item_ID"),
          'Request_reason'  =>$this->input->post("Request_reason"),
        );
        // @see registered_user_model::insert_requested_item($data), located in models folder,
        // It insert array of data to [request_item table] database
        $inserted = $this->Registered_user_model->insert_requested_item($request_item);
        // if true.
        if($inserted !=-1){
          //Message to be sent to user email.
          $data['Item_message'] = "your appeal have been received. Request ID: [".$inserted."] . \n\nYou will recive an answer from Filo System Management shortly.";
          $data['Item'] = true;
          //Send email to user.
          $this->send_email($data);
        }else{
          //if $data == -1 redirect user Reques Item tab.
          $this->dashboard('request_item');
        }
      }
      //Refresh web page and make sure user data are not stored twice or more on database.
      header("Location: " . $_SERVER['REQUEST_URI']);
    }
    //If Request data dont met set rules redirect to Request Item tab.
    $this->dashboard('request_item');

  }

  /**
  * The following method is used to direct admin between the tabs.
  * @param tab a varaibale containg tab name where user wants to view.
  */
  public function dashboard($tab){
    if($tab == 'home' || $tab == 'jewellery' || $tab == 'pet' || $tab == 'tech_devices' || $tab == 'found_item' || $tab == 'request_item' ){
      //This load @see registered_user_model class, located in models folder.
      $this->load->model('Registered_user_model');
      //It stores the chosen tab to view.
      $data['tab'] = $tab;
      //@see registered_user_model::fetch_data(), it fetches lost item table data from databse.
      $data['fetch_data'] = $this->Registered_user_model->fetch_data();
      //This load @see registered_user_view class, located in models views, it takes as @param an array @data.
      $this->load->view('Registered_user_view', $data);
    }else {
      //If none of the tab matches load erro404 from views folder.
      $this->load->view('Err404');
    }

  }
  /**
  * This method allows to end user sesssion and consequently redirect user to main page.
  */
  public function logout(){
    //Ends user session.
    $this->session->sess_destroy();
    //redirect to main page.
    redirect(' ', 'refresh');
  }

  private function send_email($data){
    //Email subject
    $subject = "Filo System";
    //if $data['Item'] == true, means email is confermation of adding an item to database
    if($data['Item']){
      $message = "Hi ".$data['User_name']." ".$data['User_surname']." ".$data['Item_message']."\n\nKind Regards,\nFilo System Management";
    }
    //if $data['Item'] == false, means email is confermation of request an item from database.
    else{
      $message = "Hi User ID:[".$data['User_ID']."] ".$data['Item_message']."\n\nKind Regards,\nFilo System Management";
    }
    //It loads email library.
    $this->load->library('email');
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = 'FiloSystem2020@gmail.com';
    $config['smtp_pass']    = 'AstonUniFilo';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not

    //Initialize email configuration
    $this->email->initialize($config);
    //Send email from FiloSystem2020@gmail.com
    $this->email->from('FiloSystem2020@gmail.com', 'Admin');
    //Send to:
    $this->email->to($data['User_email']);
    //Email subject:
    $this->email->subject($subject);
    //Email message:
    $this->email->message($message);
    //Send Email
    $this->email->send();
  }

}

?>
