<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Admin_controller class redirect admin to the chosen tab, controls,
* validate Admin approve or refuse of requests made bay users, send email of admin decision
*  to users, allows admin to change lost items data [Item name, Item image, Item Description],
* update Request table, Lost Item table and end session when admin log off.
*/
class Admin_controller extends CI_Controller {
  /**
  * Admin_controller constructor.
  */
  function __construct(){
    // Parent constructor @see CI_Controller.
    parent::__construct();
    // If admin is logged off the following ridirect user to
    // @see main_controller::index().
    if(!$this->session->userdata('admin_info')){
      redirect(' ', 'refresh');
    }
  }

  /**
  * This method show the Home tab of admin_view.php web page.
  */
  public function index()
  {
    $this->admin_dashboard('home');
  }

  /**
  * This method allows to end admin  sesssion and consequently redirect admin to main page.
  */
  public function logout(){
    $this->session->sess_destroy();
    redirect(' ', 'refresh');
  }

  /**
  * This method validate Admin decisin regarding user Requests.
  *
  * If Request Item is Approved it will sent an approval  email  to user along
  * the Request ID and it will update Request Item table as well.
  *
  * If Request Item is Refused it will sent a refusal email to user along
  * the Request ID and it will update Request Item table as well.
  *
  * Once approve and refuse operation is done it will redirect admin to User Request tab.
  */
  public function approve_or_refuse_requests(){
    //It load form_validation library.
    $this->load->library('form_validation');
    // The following sets validation rules form user Request_ID and Admin_verdict [aka admin decision].
    // All these are required to be registered, in case they are missed user will be notified through
    // error message whihc will appear underneath text bar.
    $this->form_validation->set_rules('Request_ID','Request ID', 'required|trim|max_length[11]');
    $this->form_validation->set_rules('Admin_verdict','Date', 'required|trim');

    //If given data met the set rules, run form validation.
    if($this->form_validation->run()){
      //This load @see admin_model class, located in models folder.
      $this->load->model('Admin_model');
      //Creates an array of info such as Request_ID and Admin_verdict.
      $update_request= array(
        'Request_ID'  =>$this->input->post("Request_ID"),
        'Request_approval' => $this->admin_verdict($this->input->post("Admin_verdict")),
      );
      // @see admin_model::request_decision($data), located in models folder,
      // It checks whether Request_ID is valid or not. If valid updates Request table with
      // admin decision [ Approve (yes) or refuse (no)].
      // Once update it returns a boolean which is saved in temporary variable $User_ID.
      $User_ID=$this->Admin_model->request_decision($update_request);
      if($User_ID != false){
        //Send email to user of decision.
        $this->send_email($User_ID, $update_request);
      }
      //Refresh web page and make sure user data are not stored twice or more on database.
      header("Location: " . $_SERVER['REQUEST_URI']);
    }else{
      //If Request data dont met set rules redirect to User Request tab.
      $this->admin_dashboard('user_requests');
    }
  }

  /**
  * This method allows admin to edit lost item table attribute such as Item name, place and description.
  * admin can choose to edit whichever attribute wants to change out of three. when submitting admin
  * admin must choose at least one attribute.
  *
  * This method validate editing infro [Item name, place and description].
  */
  public function update_lost_item_table(){
    //It load form_validation library.
    $this->load->library('form_validation');
    // The following sets validation rules form user Lost Item ID, Item name, Place and Description.
    // All these are required to be registered, in case they are missed user will be notified through
    // error message whihc will appear underneath text bar.
    $this->form_validation->set_rules('Lost_item_ID','Lost Item ID', 'required|trim|max_length[11]');
    $this->form_validation->set_rules('Item_name','Item Name', 'max_length[40]');
    $this->form_validation->set_rules('Place','Place', 'max_length[40]');
    $this->form_validation->set_rules('Description','Description', 'max_length[500]');

    //If given data met the set rules, run form validation.
    if($this->form_validation->run()){
      //This load @see admin_model class, located in models folder.
      $this->load->model('Admin_model');
      //Creates an array of info such as Lost_item_ID, Item_name, Place and Description.
      $update_lost_item_table= array(
        'Lost_item_ID'  =>$this->input->post("Lost_item_ID"),
        'Item_name' => $this->input->post("Item_name"),
        'Place' => $this->input->post("Place"),
        'Description' => $this->input->post("Description"),
      );
      // @see admin_model::update_lost_item_table($data), located in models folder,
      // It updates Lost Item table
      $this->Admin_model->update_lost_item_table($update_lost_item_table);
      //Refresh web page and make sure user data are not stored twice or more on database.
      header("Location: " . $_SERVER['REQUEST_URI']);
    }else{
      //If given data dont met set rules redirect to Lost Items tab.
      $this->admin_dashboard('lost_items');
    }

  }

  /**
  * The following method is used to direct admin between the tabs.
  * @param tab a varaibale containg tab name where admin wants to view.
  */
  public function admin_dashboard($tab){
    //This load @see admin_model class, located in models folder.
    $this->load->model('Admin_model');
    //It stores the chosen tab to view.
    $data['tab'] = $tab;
    //calls @see admin_model::fetch_data('request_item') which fetch user request [request id, request reason....]from database
    $data['fetch_data_user_requests'] = $this->Admin_model->fetch_data('Request_item');
    //calls @see admin_model::fetch_data('registered_user') which fetch user info [name, surnname, id.....] from database
    $data['fetch_user_info'] = $this->Admin_model->fetch_data('Registered_user');
    //calls @see admin_model:fetch_data('lost_items') which fetch lost item info [item id, item name, item colour, item description, item place, item category...] from database
    $data['fetch_lost_item_info'] = $this->Admin_model->fetch_data('Lost_items');
    //This load @see admin_view class, located in models views, it takes as @param an array @data.
    $this->load->view('Admin_view', $data);
  }

  /**
  * This change admin deciions of approval or refusal to a simple string ['yes' or 'no'].
  * @param verdict [approval or refusal].
  * @return string ['yes' or 'no'].
  */
  private function admin_verdict($verdict){
    if($verdict == 'Approve'){
      return 'yes';
    }else {
      return 'no';
    }
  }

  private function send_email($User_ID, $Request_approval_decision){
    //This load @see admin_model class, located in models folder.
    $this->load->model('Admin_model');
    //@see admin_model::fetch_user_email($data) fetches user email from database
    $User_email= $this->Admin_model->fetch_user_email($User_ID);
    //Email subject
    $subject = "Filo Request Verdict [ID: ".$Request_approval_decision['Request_ID']." ]";
    if($Request_approval_decision['Request_approval'] == 'yes'){
      //Approval message.
      $message = "Hi your request ID: ".$Request_approval_decision['Request_ID']." have been approved.\n\nKind Regards,\nFilo System Management";
    }else{
      //Refusal message.
      $message = "Hi your request ID: ".$Request_approval_decision['Request_ID']." have been refused.\n\nKind Regards,\nFilo System Management";
    }
    //It loads email library.
    $this->load->library('email');
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = 'FiloSystem2020@gmail.com';
    $config['smtp_pass']    = 'AstonUniFilo';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not

    //Initialize email configuration
    $this->email->initialize($config);
    //Send email from FiloSystem2020@gmail.com
    $this->email->from('FiloSystem2020@gmail.com', 'Admin');
    //Send to:
    $this->email->to($User_email);
    //Email subject:
    $this->email->subject($subject);
    //Email message:
    $this->email->message($message);
    //Send Email
    $this->email->send();
  }

}
?>
